package com.bco.cliente.service.impl;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.bco.cliente.entity.Cliente;
import com.bco.cliente.service.ClienteServiceLocal;
import com.bco.cliente.service.ClienteServiceRemote;

@Stateless
public class ClienteServiceBean implements ClienteServiceLocal, ClienteServiceRemote{
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public BigDecimal registrar(Cliente cliente) {
		em.persist(cliente);
		em.flush();
		return cliente.getIdcliente();
	}

}
