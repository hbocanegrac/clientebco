package com.bco.cliente.service;

import java.math.BigDecimal;

import javax.ejb.Local;

import com.bco.cliente.entity.Cliente;

@Local
public interface ClienteServiceLocal {
	
	public BigDecimal registrar(Cliente cliente);
}
