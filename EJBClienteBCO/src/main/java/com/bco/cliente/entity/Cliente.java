package com.bco.cliente.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name="CLI_CLIENTE", schema="BCO_CLIENTE")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_SEQ")
    @SequenceGenerator(sequenceName = "BCO_CLIENTE.SQ_IDCLIENTE", allocationSize = 1, name = "CLI_SEQ")
	private BigDecimal idcliente;
	
	private String apematerno;

	private String apepaterno;

	private Integer coddistrito;

	private Date fecnacimiento;

	private String nombre;

	private String nombrecompleto;

	public Cliente() {
	}

	public String getApematerno() {
		return this.apematerno;
	}

	public void setApematerno(String apematerno) {
		this.apematerno = apematerno;
	}

	public String getApepaterno() {
		return this.apepaterno;
	}

	public void setApepaterno(String apepaterno) {
		this.apepaterno = apepaterno;
	}

	public Integer getCoddistrito() {
		return this.coddistrito;
	}

	public void setCoddistrito(Integer coddistrito) {
		this.coddistrito = coddistrito;
	}

	public Date getFecnacimiento() {
		return this.fecnacimiento;
	}

	public void setFecnacimiento(Date fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}

	public BigDecimal getIdcliente() {
		return this.idcliente;
	}

	public void setIdcliente(BigDecimal idcliente) {
		this.idcliente = idcliente;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombrecompleto() {
		return this.nombrecompleto;
	}

	public void setNombrecompleto(String nombrecompleto) {
		this.nombrecompleto = nombrecompleto;
	}

}