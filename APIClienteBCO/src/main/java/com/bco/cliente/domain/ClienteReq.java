package com.bco.cliente.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ClienteReq implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@NotNull (message="Ingrese nombre completo")
	private String nombrecompleto;
	
	@NotNull(message="Ingrese fecha de nacimiento")
	private String fecnacimiento;
	
	@NotNull (message="Seleccione un distrito")
	private Integer coddistrito;
	
	@NotNull
	@Size(min=1, message="Ingrese un valor al campo direccion")
	private String direccion;
	
	public ClienteReq() {
		super();
	}
	
	public String getNombrecompleto() {
		return nombrecompleto;
	}
	
	public void setNombrecompleto(String nombrecompleto) {
		this.nombrecompleto = nombrecompleto;
	}
	
	public String getFecnacimiento() {
		return fecnacimiento;
	}
	
	public void setFecnacimiento(String fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}
		
	public Integer getCoddistrito() {
		return coddistrito;
	}
	
	public void setCoddistrito(Integer coddistrito) {
		this.coddistrito = coddistrito;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		String request = "" +
				"{" + 
				"  \"nombre\": \"" + this.getNombrecompleto() + "\"," + 
				"  \"fecnacimiento\": \"" + this.getFecnacimiento() + "\"," + 
				"  \"coddistrito\": \"" + this.getCoddistrito() + "\"," + 
				"  \"direccion\": \"" + this.getDireccion() + "\"" + 
				"}";
		return request;
	}
	
	
}
