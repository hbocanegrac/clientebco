package com.bco.cliente.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bco.cliente.domain.ClienteReq;
import com.bco.cliente.entity.Cliente;
import com.bco.cliente.service.ClienteServiceLocal;

@Path("/")
public class ClienteEndpoint {
	
	@EJB
	private ClienteServiceLocal clienteService;
	
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrar(@Valid ClienteReq request) {
				
		Cliente nuevo = new Cliente();
		nuevo.setNombrecompleto(request.getNombrecompleto());
		
		try {
			nuevo.setFecnacimiento(dateFormat.parse(request.getFecnacimiento()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		nuevo.setCoddistrito(request.getCoddistrito());
		clienteService.registrar(nuevo);
		
		return Response.ok().build();
	}
}
