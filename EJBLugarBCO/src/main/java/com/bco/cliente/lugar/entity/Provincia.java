package com.bco.cliente.lugar.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="LUG_PROVINCIA", schema="BCO_CLIENTE")
@NamedQueries({
    @NamedQuery(name = "Provincia.findAll", query="SELECT p FROM Provincia p"),
    @NamedQuery(name = "Provincia.findByDepartamento", query = "SELECT p FROM Provincia p WHERE p.coddepartamento = :coddepartamento")})
public class Provincia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long codprovincia;

	private long coddepartamento;

	private String dscprovincia;

	public Provincia() {
	}

	public long getCodprovincia() {
		return this.codprovincia;
	}

	public void setCodprovincia(long codprovincia) {
		this.codprovincia = codprovincia;
	}

	public long getCoddepartamento() {
		return this.coddepartamento;
	}

	public void setCoddepartamento(long coddepartamento) {
		this.coddepartamento = coddepartamento;
	}

	public String getDscprovincia() {
		return this.dscprovincia;
	}

	public void setDscprovincia(String dscprovincia) {
		this.dscprovincia = dscprovincia;
	}

}