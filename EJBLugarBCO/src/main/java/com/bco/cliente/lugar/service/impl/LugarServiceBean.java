package com.bco.cliente.lugar.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.bco.cliente.lugar.entity.Departamento;
import com.bco.cliente.lugar.entity.Distrito;
import com.bco.cliente.lugar.entity.Provincia;
import com.bco.cliente.lugar.service.LugarServiceLocal;
import com.bco.cliente.lugar.service.LugarServiceRemote;

@Stateless
public class LugarServiceBean implements LugarServiceLocal, LugarServiceRemote {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Departamento> lstDepartamento() {
		
		List<Departamento> lista = 
				em.createNamedQuery("Departamento.findAll").getResultList();
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Provincia> lstProvincia(long coddepartamento) {
		
		List<Provincia> lista = em.createNamedQuery("Provincia.findByDepartamento")
			    .setParameter("coddepartamento", coddepartamento)
			    .getResultList();
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Distrito> lstDistrito(long codprovincia) {
		
		List<Distrito> lista = em.createNamedQuery("Distrito.findByProvincia")
			    .setParameter("codprovincia", codprovincia)
			    .getResultList();
		
		return lista;
	}

}
