package com.bco.cliente.lugar.service;

import java.util.List;

import javax.ejb.Local;
import com.bco.cliente.lugar.entity.Departamento;
import com.bco.cliente.lugar.entity.Distrito;
import com.bco.cliente.lugar.entity.Provincia;

@Local
public interface LugarServiceLocal {
	
	public List<Departamento> lstDepartamento();
	public List<Provincia> lstProvincia(long coddepartamento);
	public List<Distrito> lstDistrito(long codprovincia);
}
