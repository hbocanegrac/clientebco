package com.bco.cliente.lugar.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="LUG_DEPARTAMENTO", schema="BCO_CLIENTE")
@NamedQuery(name="Departamento.findAll", query="SELECT l FROM Departamento l")
public class Departamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long coddepartamento;

	private String dscdepartamento;

	public Departamento() {
	}

	public long getCoddepartamento() {
		return this.coddepartamento;
	}

	public void setCoddepartamento(long coddepartamento) {
		this.coddepartamento = coddepartamento;
	}

	public String getDscdepartamento() {
		return this.dscdepartamento;
	}

	public void setDscdepartamento(String dscdepartamento) {
		this.dscdepartamento = dscdepartamento;
	}

}