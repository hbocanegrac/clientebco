package com.bco.cliente.lugar.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="LUG_DISTRITO", schema="BCO_CLIENTE")
@NamedQueries({
    @NamedQuery(name = "Distrito.findAll", query="SELECT d FROM Distrito d"),
    @NamedQuery(name = "Distrito.findByProvincia", query = "SELECT d FROM Distrito d WHERE d.codprovincia = :codprovincia")})
public class Distrito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long coddistrito;

	private long codprovincia;

	private String dscdistrito;

	public Distrito() {
	}

	public long getCoddistrito() {
		return this.coddistrito;
	}

	public void setCoddistrito(long coddistrito) {
		this.coddistrito = coddistrito;
	}

	public long getCodprovincia() {
		return this.codprovincia;
	}

	public void setCodprovincia(long codprovincia) {
		this.codprovincia = codprovincia;
	}

	public String getDscdistrito() {
		return this.dscdistrito;
	}

	public void setDscdistrito(String dscdistrito) {
		this.dscdistrito = dscdistrito;
	}

}