package com.bco.cliente.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bco.cliente.lugar.entity.Departamento;
import com.bco.cliente.lugar.entity.Distrito;
import com.bco.cliente.lugar.entity.Provincia;
import com.bco.cliente.lugar.service.LugarServiceLocal;

@Path("/")
public class LugarEndpoint {
	
	@EJB
	private LugarServiceLocal lugarService;
	
	@POST
	@Path("/lstdepartamento")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lstDepartamento() {
		
		List<Departamento> lista = lugarService.lstDepartamento();
		return Response.status(javax.ws.rs.core.Response.Status.OK)
				.entity(lista).build();
	}
	
	@POST
	@Path("/lstprovincia/{coddepartamento}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lstProvincia(@PathParam("coddepartamento") long coddepartamento) {

		List<Provincia> lista = lugarService.lstProvincia(coddepartamento);
		return Response.status(javax.ws.rs.core.Response.Status.OK)
				.entity(lista).build();
	}
	
	@POST
	@Path("/lstdistrito/{codprovincia}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response lstdistrito(@PathParam("codprovincia") long codprovincia) {
		
		List<Distrito> lista = lugarService.lstDistrito(codprovincia);
		return Response.status(javax.ws.rs.core.Response.Status.OK)
				.entity(lista).build();
	}
}
